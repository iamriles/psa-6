import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.text.*;
import java.util.*;
import java.util.List; // resolves problem with java.awt.List and java.util.List

/**
 * A class that represents a picture.  This class inherits from 
 * SimplePicture and allows the student to add functionality to
 * the Picture class.  
 * 
 * Copyright Georgia Institute of Technology 2004-2005
 * @author Barbara Ericson ericson@cc.gatech.edu
 */
public class Picture extends SimplePicture 
{
  ///////////////////// constructors //////////////////////////////////
  
  /**
   * Constructor that takes no arguments 
   */
  public Picture ()
  {
    /* not needed but use it to show students the implicit call to super()
     * child constructors always call a parent constructor 
     */
    super();  
  }
  
  /**
   * Constructor that takes a file name and creates the picture 
   * @param fileName the name of the file to create the picture from
   */
  public Picture(String fileName)
  {
    // let the parent class handle this fileName
    super(fileName);
  }
  
  /**
   * Constructor that takes the width and height
   * @param width the width of the desired picture
   * @param height the height of the desired picture
   */
  public Picture(int width, int height)
  {
    // let the parent class handle this width and height
    super(width,height);
  }
  
  /**
   * Constructor that takes a picture and creates a 
   * copy of that picture
   */
  public Picture(Picture copyPicture)
  {
    // let the parent class do the copy
    super(copyPicture);
  }
  
  /**
   * Constructor that takes a buffered image
   * @param image the buffered image to use
   */
  public Picture(BufferedImage image)
  {
    super(image);
  }
  
  ////////////////////// methods ///////////////////////////////////////
  
  /**
   * Method to return a string with information about this picture.
   * @return a string with information about the picture such as fileName,
   * height and width.
   */
  public String toString()
  {
    String output = "Picture, filename " + getFileName() + 
      " height " + getHeight() 
      + " width " + getWidth();
    return output;
    
  }

  public void chromaKeyGreen (Color targetColor,
                               double thresh,
                               Picture source)
  {
    Pixel sourcePixel = null;
    Pixel targetPixel = null;
    for (int targetx = 0,
         sourcex = 0; sourcex < source.getWidth();
         targetx++, sourcex++)
    {
      for (int targety = 0,
           sourcey = 0; targety < this.getHeight();
           targety++, sourcey++)
      {
        //bounds checking
        if ((sourcex < source.getWidth() && sourcey < source.getHeight()) &&
            (targetx < this.getWidth() && targety < this.getHeight())
      ){
          //color checking
          sourcePixel = source.getPixel(sourcex, sourcey);
          targetPixel = this.getPixel(targetx, targety);
          //color checking
          if (targetPixel.colorDistance(targetColor) < thresh)
          targetPixel.setColor(sourcePixel.getColor());
        }
      }
    }
  }
  
  public void chromaKeyGray (Color targetColor,
                               double thresh,
                               Picture source)
  {
    Pixel sourcePixel = null;
    Pixel targetPixel = null;
    
    //nested loops in case more position
    for (int targetx = 0,
         sourcex = 0; sourcex < source.getWidth();
         targetx++, sourcex++)
    {
      for (int targety = 0,
           sourcey = 0; targety < this.getHeight();
           targety++, sourcey++)
      {
        //bounds checking
        if ((sourcex < source.getWidth() && sourcey < source.getHeight()) &&
            (targetx < this.getWidth() && targety < this.getHeight())
      ){
          //color checking
          sourcePixel = source.getPixel(sourcex, sourcey);
          targetPixel = this.getPixel(targetx, targety);
          //color checking
          if (targetPixel.colorDistance(targetColor) < thresh)
          targetPixel.setColor(sourcePixel.getColor());
        }
      }
    }
  }
  public static void main(String[] args) 
  {
    String picturesLoc = "Pictures\\";
    Picture greenscreen = new Picture(picturesLoc + "greenscreen.jpg");
    Picture space = new Picture(picturesLoc + "space.jpg");
    Picture pattern = new Picture(picturesLoc + "pattern.jpg");
    greenscreen.explore();
    
    //grab green values
    Color targetGreen1 = greenscreen.getPixel(786, 180).getColor();
    Color targetGreen2 = greenscreen.getPixel(364, 296).getColor();
    
    //replace green with space(2 passes neede)
    greenscreen.chromaKeyGreen(targetGreen1,
                               90,
                               space);   
    greenscreen.chromaKeyGreen(targetGreen2,
                               35,
                               space);
    
    //grab grey samples
    Color targetGrey1 = greenscreen.getPixel(437, 475).getColor();
    Color targetGrey2 = greenscreen.getPixel(355, 897).getColor();
    
    //replace grey
    greenscreen.chromaKeyGreen(targetGrey1,
                               30,
                               pattern);
    greenscreen.chromaKeyGreen(targetGrey2,
                               25,
                               pattern);
    greenscreen.explore();
  }
} // this } is the end of class Picture, put all new methods before this
 