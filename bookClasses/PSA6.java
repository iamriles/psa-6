/*
 * File: PSA6.java
 *
 * Created by: Riles LeRoy
 *
 *
*/
public class PSA6
{
  public static void main(String [] args) {
    //file managment
    String picturesLoc = "Pictures\\";
    Picture greenscreen = new Picture(picturesLoc + "greenscreen.jpg");
    Picture space = new Picture(picturesLoc + "space.jpg");
    Picture pattern = new Picture(picturesLoc + "pattern.jpg");
    greenscreen.explore();
    
    //grab green values
    Pixel targetGreen1 = greenscreen.getPixel(786, 180);
    Pixel targetGreen2 = greenscreen.getPixel(364, 296);
    
    //replace green with space(2 passes neede)
    greenscreen.chromaKeyGreen(targetGreen1.getColor(),
                               90,
                               space);   
    greenscreen.chromaKeyGreen(targetGreen2.getColor(),
                               35,
                               space);
    
    //grab grey samples
    Pixel targetGrey1 = greenscreen.getPixel(437, 475);
    Pixel targetGrey2 = greenscreen.getPixel(355, 897);
    
    //replace grey
    greenscreen.chromaKeyGreen(targetGrey1.getColor(),
                               30,
                               pattern);
    greenscreen.chromaKeyGreen(targetGrey2.getColor(),
                               25,
                               pattern);
    greenscreen.explore();
  }
}